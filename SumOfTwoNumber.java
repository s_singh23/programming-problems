
/**
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]

**/

class SumOfTwoNumber {
    /**public int[] twoSum(int[] nums, int target) {
        int opt[]=new int[2];
        for(int i=0;i<nums.length;i++){
            for(int j=1;j<nums.length;j++){
               int sum=nums[i]+nums[j];    
                if(sum==target){
                    opt[0]=i;
                    opt[1]=j;
                    System.out.println(opt);
                }                
            }
        }
        return opt;
    }**/
    
     public int[] twoSum(int[] nums, int target) {
        HashMap<Integer,Integer> mp= new HashMap<Integer,Integer>();
        for(int i=0;i<nums.length;i++){
            Integer trg=(Integer)target-nums[i];
            if(mp.containsKey(trg)){
                int[] opt={mp.get(trg),i};
                return opt;
            }
            mp.put(nums[i],i);
        }
        return null;
    }
}